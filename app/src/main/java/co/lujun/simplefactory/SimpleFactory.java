package co.lujun.simplefactory;

/**
 * Created by lujun on 2015/8/31.
 */
public class SimpleFactory {//工厂角色

    public Car createCar(String name){
        return new Factory().create(name);
    }

    public class Factory{
        public  Car create(String name){
            Car car = null;
            try{
                car = (Car) Class.forName("co.lujun.simplefactory." + name).newInstance();
            }catch (InstantiationException e){
                e.printStackTrace();
            }catch (IllegalAccessException e){
                e.printStackTrace();
            }catch (ClassNotFoundException e){
                e.printStackTrace();
            }
            return car;
        }
    }

    public interface Car {//抽象产品角色
        void start();
        void stop();
    }
}
