package co.lujun.simplefactory;

import android.util.Log;

/**
 * Created by lujun on 2015/8/31.
 */
public class Benz implements SimpleFactory.Car {

    private final static String TAG = "tag";

    @Override
    public void start() {
        Log.i(TAG, "Benz is start.");
    }

    @Override
    public void stop() {
        Log.i(TAG, "Benz is stop.");
    }
}