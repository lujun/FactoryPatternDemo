package co.lujun.factorypatterndemo;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;

import co.lujun.abstractfactory.Factory;
import co.lujun.factorymethod.AudiFactory;
import co.lujun.factorymethod.BenzFactory;
import co.lujun.factorymethod.Car;
import co.lujun.factorymethod.ToyotaFactory;
import co.lujun.simplefactory.SimpleFactory;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //simple factory test
        /*SimpleFactory factory = new SimpleFactory();
        SimpleFactory.Car car = factory.createCar("Audi");
        if (car != null){
            car.start();
            car.stop();
        }*/

        //factory method test
        /*AudiFactory audiFactory = new AudiFactory();
        Car audi = audiFactory.create();
        audi.run();
        BenzFactory benzFactory = new BenzFactory();
        Car benz = benzFactory.create();
        benz.run();
        ToyotaFactory toyotaFactory = new ToyotaFactory();
        Car toyota = toyotaFactory.create();
        toyota.run();*/

        //abstract factory test
        Factory factory = new Factory();
        factory.createCar().run();
        factory.createPeople().speak();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
