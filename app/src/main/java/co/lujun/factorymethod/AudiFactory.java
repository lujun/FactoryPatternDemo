package co.lujun.factorymethod;

/**
 * Created by lujun on 2015/8/31.
 */
public class AudiFactory extends FactoryMethod {

    @Override
    public Car create() {
        return new Audi();
    }
}
