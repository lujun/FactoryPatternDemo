package co.lujun.factorymethod;

/**
 * Created by lujun on 2015/8/31.
 */
public class ToyotaFactory extends FactoryMethod {

    @Override
    public Car create() {
        return new Toyota();
    }
}
