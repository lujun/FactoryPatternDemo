package co.lujun.factorymethod;

import android.util.Log;

/**
 * Created by lujun on 2015/8/31.
 */
public class Audi implements Car {

    private final static String TAG = "tag";

    @Override
    public void run() {
        Log.i(TAG, "Audi is run!");
    }
}
