package co.lujun.factorymethod;

/**
 * Created by lujun on 2015/8/31.
 */
public interface Car {
    void run();
}
