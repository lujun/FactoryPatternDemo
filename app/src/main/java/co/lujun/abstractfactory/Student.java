package co.lujun.abstractfactory;

import android.util.Log;

/**
 * Created by lujun on 2015/8/31.
 */
public class Student implements People {//具体产品角色

    private final static String TAG = "tag";

    @Override
    public void speak() {
        Log.i(TAG, "I'm a student!");
    }
}
