package co.lujun.abstractfactory;

/**
 * Created by lujun on 2015/8/31.
 */
public abstract class AbstractFactory {//抽象工厂，两个产品族
    public abstract Car createCar();
    public abstract People createPeople();
}
