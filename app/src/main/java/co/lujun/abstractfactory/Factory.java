package co.lujun.abstractfactory;

/**
 * Created by lujun on 2015/8/31.
 */
public class Factory extends AbstractFactory {//具体工厂

    @Override
    public Car createCar() {
        return new Audi();
    }

    @Override
    public People createPeople() {
        return new Student();
    }
}
